#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:21310764:3e7f9327c8dcd4b2509b33b8ff3e099c66986e4a; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:20544808:651ee4b58c7224f75ecab4a9d68fc1f5e6b06265 EMMC:/dev/block/bootdevice/by-name/recovery 3e7f9327c8dcd4b2509b33b8ff3e099c66986e4a 21310764 651ee4b58c7224f75ecab4a9d68fc1f5e6b06265:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
